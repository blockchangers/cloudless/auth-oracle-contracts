import { BuidlerConfig, usePlugin } from "@nomiclabs/buidler/config";

usePlugin("@nomiclabs/buidler-waffle");
usePlugin("@nomiclabs/buidler-ethers");
usePlugin("buidler-typechain");
// usePlugin("buidler-gas-reporter");
// usePlugin("@nomiclabs/buidler-solpp");

const config: BuidlerConfig = {
  defaultNetwork: "local",
  networks: {
    local: {
      url: " http://127.0.0.1:8545/",
    },
    buidlerevm: {
      // gas: "auto",
      // blockGasLimit: 5000000000,
      // loggingEnabled: true,
    },
    grefsen: {
      url:
        "https://e0qzjtvk2c:F38R7jlOwc71Idz0KO-sOdI91qg0HkzibZBDkufwbQA@e0yoqxkx27-e0kr6bistz-rpc.de0-aws.kaleido.io",
      // gasMultiplier: 2,
      // port: 8545,
      // gas: 5000000,
      // gasPrice: 5e9,
    },
    brreg: {
      url:
        "https://u1qdua80h5:Er0LWdZuKqOza22YNQKhtdFCbqRzhzGCRhuZgrtHZ9s@u1txh1ent0-u1ieecy018-rpc.us1-azure.kaleido.io",
    },
    rinkeby: {
      url: "https://rinkeby.infura.io/v3/499c90502ea347c3971243e0e0f7172e",
      accounts: {
        mnemonic:
          "gas blush witness leopard voyage regret napkin mail onion pitch gather soon",
      },
    },
  },
  solc: {
    version: "0.5.5",
    // optimizer: {
    //   enabled: true,
    //   runs: 200,
    // },
  },
  typechain: {
    outDir: "lib/",
    target: "ethers",
  },
  // gasReporter: {
  //   currency: "NOK",
  //   gasPrice: 5,
  // },
  // solpp: {
  //   cwd: "contracts",
  //   tolerant: true,
  // },
};

export default config;
