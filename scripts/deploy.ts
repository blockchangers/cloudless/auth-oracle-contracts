import { ethers } from "@nomiclabs/buidler";
import { Signer } from "ethers";
import { BigNumber } from "ethers/utils";
import * as AuthOracleJSON from "./../artifacts/AuthOracle.json";

async function main() {
  // // Do eth stuff
  // const privateKey =
  //   process.env.PRIVATE_KEY ||
  //   "0xad6f29b5b5285c8137787710ebdcc5ee16a3f09598a798bee470e158ade704fc"; // 0xbb1c879cb7f5129ba026DfE1E5f30979D7978A65
  // const wallet = new ethers.Wallet(privateKey);
  // const provider = new ethers.providers.JsonRpcProvider(
  //   "https://e0mcmsxgkk:zdh5CFSvVXzCUF8e3-QbN5tiVTBuekQYFgNxbMSJ68c@e0fsgog2j5-e0h0jkl669-rpc.de0-aws.kaleido.io"
  // );

  // const signer = wallet.connect(provider);
  // const test = await provider.getNetwork().catch((err) => {
  //   console.log(err);
  // });
  // console.log("test ====>", test);
  // let factory = new ethers.ContractFactory(
  //   AuthOracleJSON.abi,
  //   AuthOracleJSON.bytecode,
  //   signer
  // );
  // let contract = await factory.deploy();
  // contract.deployed();
  // console.log("Contract has been deployed", contract.address);

  const address = await getAddress();
  console.log("Deploy address   =>", address);
  const factory = await ethers.getContract("AuthOracle");
  let contract = await factory.deploy();
  console.log(
    "REACT_APP_AUTH_ORACLE='" + contract.address + "'"
  );
  console.log("hash             => ", contract.deployTransaction.hash);
  await contract.deployed();
  console.log("Contract has been deployed");
}

const getSigner = async (): Promise<Signer> => {
  return (await ethers.getSigners())[0];
};

const getAddress = async (): Promise<string> => {
  return await (await getSigner()).getAddress();
};

const getBalance = async (address: string): Promise<BigNumber> => {
  return await ethers.provider.getBalance(address);
};

const getTransactionCount = async (address: string): Promise<number> => {
  return await ethers.provider.getTransactionCount(address);
};

main()
  .then(() => process.exit(0))
  .catch((error) => {
    // Object.keys(error).forEach(key => {
    //   console.log(key);
    // });
    console.log(error.reason);
    console.log(error.code);
    process.exit(1);
  });
