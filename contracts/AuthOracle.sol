pragma solidity ^0.5.5;

import "@openzeppelin/contracts/ownership/Ownable.sol";


contract AuthOracle is Ownable {
    // Yes, we know this can be brute forced (very easily) and we will not use this solution in production.
    mapping(bytes32 => address) internal _ssnToAddress;
    mapping(address => bytes32) internal _addressToSsn;
    mapping(bytes32 => bytes32) internal _ssnToName;
    mapping(address => bytes32) internal _addressToName;

    event NewSsnToAddress(address indexed adr, bytes32 indexed ssn);

    constructor() public {
        _transferOwnership(0xbb1c879cb7f5129ba026DfE1E5f30979D7978A65);
    }

    function set_ssn_to_address(address adr, bytes32 ssn, bytes32 name)
        external
        onlyOwner()
    {
        _ssn_to_address(adr, ssn, name);
    }

    function suggest_ssn_to_address(bytes32 ssn, bytes32 name) external {
        // yea, this is only for testing enviroments
        address randomishAddress = address(
            uint160(
                uint256(
                    keccak256(abi.encodePacked(ssn, blockhash(block.number)))
                )
            )
        );
        _ssn_to_address(randomishAddress, ssn, name);
    }

    function _ssn_to_address(address adr, bytes32 ssn, bytes32 name) internal {
        require(adr != address(0), "ADDRESS can not be empty");
        require(ssn != bytes32(0), "SSN can not be empty");
        require(ssn != bytes32(0), "NAME can not be empty");
        require(
            _addressToSsn[adr] == bytes32(0),
            "Address has been allocated before"
        );
        _ssnToAddress[ssn] = adr;
        _addressToSsn[adr] = ssn;
        _ssnToName[ssn] = name;
        _addressToName[adr] = name;
        emit NewSsnToAddress(adr, ssn);
    }

    function get_address_from_ssn(bytes32 ssn)
        external
        view
        returns (address adr)
    {
        require(ssn != bytes32(0), "SSN can not be empty");
        return _ssnToAddress[ssn];
    }

    function get_ssn_from_address(address adr)
        external
        view
        returns (bytes32 ssn)
    {
        require(adr != address(0), "ADDRESS can not be empty");
        return _addressToSsn[adr];
    }

    function get_name_from_address(address adr)
        external
        view
        returns (bytes32 name)
    {
        require(adr != address(0), "ADDRESS can not be empty");
        return _addressToName[adr];
    }

    function get_name_from_ssn(bytes32 ssn)
        external
        view
        returns (bytes32 name)
    {
        require(ssn != bytes32(0), "SSN can not be empty");
        return _ssnToName[ssn];
    }

    function address_owns_ssn(address adr, bytes32 ssn)
        external
        view
        returns (bool attested)
    {
        require(adr == _ssnToAddress[ssn], "msg.sender does not hold SSN.");
        return true;
    }

    function is_address_attested(address adr) external view returns (bool) {
        require(
            _addressToSsn[adr] != bytes32(0) &&
                _ssnToAddress[_addressToSsn[adr]] == adr,
            "msg.sender must not have empty ssn && ssn must be set to msg.sender"
        );
        return true;
    }
}
